function filter(filtre){

	var i, button, allbuttons;

	//we check if the selected filter was already active
	//if yes, we make the filter button inactive and disactivate filter
	//if no, we make the filter button active and activate filter
	button = event.target;
	if(button.classList.contains("active")){
		button.classList.remove("active");
		removeFilters();
	}
	else{
		//we first check if other buttons were active to disactivate them
		allbuttons = document.getElementsByTagName("button");
		for (i=0; i<allbuttons.length; i++){
			if(allbuttons[i].classList.contains("active")){
				allbuttons[i].classList.remove("active");
			}
		}
		button.classList.add("active");
		activateFilter(filtre);
	}
}

function activateFilter(filtre){
	var itemsToHide, itemsToShow, i;

	//first make all ideas semi-transparent then reveal only the filtered ones.
	itemsToHide = document.getElementsByClassName("idea");
	itemsToShow = document.getElementsByClassName(filtre);
	for (i=0; i<itemsToHide.length; i++){
		itemsToHide[i].style.opacity = "0.3";
	}
	for (i=0; i<itemsToShow.length; i++){
		itemsToShow[i].parentNode.style.opacity = "1";
	}
}

function removeFilters(){
	var itemsToShow, i;

	itemsToShow = document.getElementsByClassName("idea");
	for (i=0; i<itemsToShow.length; i++){
		itemsToShow[i].style.opacity = "1";
	}

}